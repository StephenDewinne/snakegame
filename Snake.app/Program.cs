﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake.app
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Fonction Génératrice de nombre aléatoire

            int RandomNumber(int min, int max)
            {
                Random random = new Random();
                return random.Next(min, max);
            }

            #endregion

            #region Fonction Génératrice de position (X,Y) pour la pomme

            int[] AfficherPomme(int[] positionpomme)
            {
                positionpomme[0] = RandomNumber(3, 60);

                if (positionpomme[0] % 2 == 0)
                    positionpomme[0]++;

                positionpomme[1] = RandomNumber(4, 30);

                return positionpomme;
            }

            #endregion

            #region Fonction Dessinant la zone de jeu

            void Dessin()
            {
                Console.WriteLine();
                Console.WriteLine("┌------------------------------------------------------------┐");
               // Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("└------------------------------------------------------------┘");
            }

            #endregion

            #region Déclaration des variables

            ConsoleKeyInfo touche = new ConsoleKeyInfo();
            List<string> snake = new List<string>();
            snake.Add("■");

            List<int> posSnakeX = new List<int>();
            List<int> posSnakeY = new List<int>();

            int positionX = 31;
            int positionY = 15;

            posSnakeX.Add(positionX);
            posSnakeY.Add(positionY);

            int[] positions = new int[2];
            positions[0] = positionX;
            positions[1] = positionY;

            int directionX = 0;
            int directionY = 0;
            int direction = 0;
            bool jeu = true;
            bool gameover = false;

            bool pomme = false;

            int[] positionPomme = new int[2];

            #endregion

            #region Affichage du Game Over

            bool gameOver()
            {

                gameover = true;

                System.Threading.Thread.Sleep(1000);

                Console.Clear();

                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");

                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"              _____             _                          ");
                Console.WriteLine(@"              |  __ \           | |                        ");
                Console.WriteLine(@"              | |__) |__ _ __ __| |_   _                   ");
                Console.WriteLine(@"              |  ___/ _ \ '__/ _` | | | |                  ");
                Console.WriteLine(@"              | |  |  __/ | | (_| | |_| |_ _ _             ");
                Console.WriteLine(@"              |_|   \___|_|  \__,_|\__,_(_|_|_)            ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                //Console.WriteLine(@$"                    Score : {score}                     ");
                Console.WriteLine(@"     Appuyez sur n'importe quelle touche pour quitter!     ");

                Console.ReadKey();
                return gameover;
            }

            #endregion

            while (jeu)
            {
                Console.SetCursorPosition(0, 0);
                Dessin();

                positionX += directionX;
                positionY += directionY;

                #region Enregistrement de la position (X,Y) du snake

                if ((positions[0] != positionX) || positions[1] != positionY)
                {
                    if (positionX < 0) positionX = 0;
                    posSnakeX.Add(positionX);
                    posSnakeY.Add(positionY);

                    positions[0] = positionX;
                    positions[1] = positionY;
                }

                #endregion

                #region Positionnement et affichage de la pomme

                if (pomme == false)
                {
                    positionPomme = AfficherPomme(positionPomme);
                    pomme = true;
                }

                Console.SetCursorPosition(positionPomme[0], positionPomme[1]);
                //Console.Write("∅");
                Console.Write("*");

                #endregion

                #region Calcul et affichage du snake

                for (int i = 0; i < snake.Count; i++)
                {
                    Console.SetCursorPosition(posSnakeX[posSnakeX.Count - 1 - i], posSnakeY[posSnakeY.Count - 1 - i]);
                    Console.Write("■");
                }

                #endregion

                #region Détection de la touche appuyée

                if (Console.KeyAvailable)
                {
                    touche = Console.ReadKey(true);
                    direction = (int)touche.Key;
                }

                #endregion

                #region Changement de direction selon la touche appuyée

                //HAUT
                if (direction == 38 && directionY != 1)
                {
                    directionX = 0;
                    directionY = -1;
                }

                //BAS
                else if (direction == 40 && directionY != -1)
                {
                    directionX = 0;
                    directionY = 1;
                }

                //GAUCHE
                else if (direction == 37 && directionX != 2)
                {
                    directionX = -2;
                    directionY = 0;
                }

                //DROITE
                else if (direction == 39 && directionX != -2)
                {
                    directionX = 2;
                    directionY = 0;
                }

                #endregion

                #region Détection de collision avec les murs

                if (positionX < 1 || positionX > 60 || positionY < 2 || positionY > 30)
                {
                    gameOver();
                }

                if (gameover)
                {
                    break;
                }

                #endregion

                #region Détection de collision avec la pomme

                //Collision avec la pomme

                if (positionPomme[0] == positionX && positionPomme[1] == positionY)
                {
                    snake.Add("■");
                    pomme = false;
                }

                #endregion

                Console.SetCursorPosition(0, 0);
                System.Threading.Thread.Sleep(50);
            }
        }
    }
}

/*
 * Choses à faire :
 * - Créer un menu pour choisir la difficulté et afficher les high scores
 * - Détecter les collisions du serpent avec sa propre queue
 * - Inserer une variable score qui varie en fonction de la difficulté
 * - créer un fichier qui sauvegarde les 3 meilleurs scores
*/
